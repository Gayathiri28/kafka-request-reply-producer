package com.ztech.kafka.models;

public class Response<T> {
	T responseBody;
	boolean isSuccess;

	public Response() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Response(T responseBody, boolean isSuccess) {
		super();
		this.responseBody = responseBody;
		this.isSuccess = isSuccess;
	}

	public T getResponseBody() {
		return responseBody;
	}

	public void setResponseBody(T responseBody) {
		this.responseBody = responseBody;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	@Override
	public String toString() {
		return "Response [responseBody=" + responseBody + ", isSuccess=" + isSuccess + "]";
	}

}
