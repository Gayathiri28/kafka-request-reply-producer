package com.ztech.kafka.controllers;

import java.util.concurrent.ExecutionException;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.requestreply.ReplyingKafkaTemplate;
import org.springframework.kafka.requestreply.RequestReplyFuture;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ztech.kafka.avro.ApplicationError;
import com.ztech.kafka.avro.ApplicationResponse;
import com.ztech.kafka.avro.ApplicationStatus;
import com.ztech.kafka.avro.Student;
import com.ztech.kafka.models.AppError;
import com.ztech.kafka.models.ApplicationSubmissionResponse;
import com.ztech.kafka.models.Response;

@RestController
public class StudentApplicationController {
	
	@Autowired
	ReplyingKafkaTemplate<String, Student, ApplicationResponse> kafkaTemplate;

	@Value("${kafka.topic.request-topic}")
	String requestTopic;

	@Value("${kafka.topic.reply-topic}")
	String replyTopic;

	@ResponseBody
	@PostMapping(value = "/submit")
	public Response<?> process(@RequestBody Student request) throws InterruptedException, ExecutionException {
		try {
			ProducerRecord<String, Student> record = new ProducerRecord<String, Student>(requestTopic, request);
			record.headers().add(new RecordHeader(KafkaHeaders.REPLY_TOPIC, replyTopic.getBytes()));

			RequestReplyFuture<String, Student, ApplicationResponse> sendAndReceive = kafkaTemplate
					.sendAndReceive(record);

			SendResult<String, Student> sendResult = sendAndReceive.getSendFuture().get();
			sendResult.getProducerRecord().headers()
					.forEach(header -> System.out.println(header.key() + ":" + header.value().toString()));

			ConsumerRecord<String, ApplicationResponse> consumerRecord = sendAndReceive.get();
			
			ApplicationResponse appResponse = consumerRecord.value();
			System.out.println("consumer reply" + consumerRecord.value());

			
			if(appResponse.getIsSuccess()) {
				ApplicationStatus appStatus = (ApplicationStatus) appResponse.getResponseBody();
				ApplicationSubmissionResponse applicationSubmissionResponse = 
						new ApplicationSubmissionResponse(String.valueOf(appStatus.getStatus()), String.valueOf(appStatus.getDescription()));
				Response<ApplicationSubmissionResponse> response = new Response<>();
				response.setResponseBody(applicationSubmissionResponse);
				return response;
			} else {
				ApplicationError applicationError = (ApplicationError) appResponse.getResponseBody();
				AppError appError = new AppError(String.valueOf(applicationError.getErrorCode()),String.valueOf(applicationError.getErrorDescription()));
				Response<AppError> response = new Response<>();
				response.setResponseBody(appError);
				return response;
			}
			

		} catch (Exception e) {
			System.out.println("Exception" + e.getMessage());
			return null;
		}
	}

}
