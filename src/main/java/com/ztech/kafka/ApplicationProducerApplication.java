package com.ztech.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationProducerApplication.class, args);
	}

}
