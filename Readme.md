1. This is a sample application demonstrating Request Reply in Kafka
2. Clone Kafka Request Reply Producer and Consumer
3. In the consumer application, update the couchbase config with the bucket and credentials information
4. Make sure that the kafka brokers and the schema registry are running
5. Run the Producer and Consumer application
6. Start by hitting the endpoint POST "/submit" of the producer
    Request:
    {
	"firstName" : "",
	"lastName": "",
	"email" : "",
	"address" : {
		"houseAddress" : " ",
		"streetAddress": "",
		"city": "",
		"state": "",
		"postalCode": "",
		"country": ""
	},
	"fileURL" : ""
    }
    
7. The consumer application should be able to read the published message,update the couchbase and return the response